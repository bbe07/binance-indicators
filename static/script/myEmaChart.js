// function renderChart(data, labels) {
//     var ctx = document.getElementById("myChart").getContext('2d');
//     var myChart = new Chart(ctx, {
//         type: 'line',
//         data: {
//             labels: labels,
//             datasets: [{
//                 label: 'This week',
//                 data: data,
//             }]
//         },
//     });
// }

// $("#renderBtn").click(
//     function () {
//         data = [20000, 14000, 12000, 15000, 18000, 19000, 22000];
//         labels =  ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
//         renderChart(data, labels);
//     }
// );

function renderChart(dates,closing20,closing50,symbol) {
    var ctx = document.getElementById("canvas").getContext('2d');
    var config = {
        type: 'line',
        data: {
            labels: dates,
            datasets: [{
                label: 'Closing 20',
                backgroundColor: window.chartColors.green,
                borderColor: window.chartColors.green,
                data: closing20,
                fill: false,
            }, {
                label: 'Closing 50',
                fill: false,
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.red,
                data: closing50,
            }]
        },
        options: {
            maintainAspectRatio: false,
            responsive: true,
            title: {
                display: true,
                text: symbol + ' EMA Cross Line Chart'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Date Time'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            }
        }
    };

    var canvas = new Chart(ctx,config);
}

window.onload = function() {
    //data = [20000, 14000, 12000, 15000, 18000, 19000, 22000];
    dates = [];
    closing20 = [];
    closing50 = [];

    //labels =  ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
    //alert($(location).attr('href'));
    //alert($(location).attr('search'));
    
    let searchParams = new URLSearchParams(window.location.search);
    if(!searchParams.has('symbol')){
        alert("No symbols declared in the URL field!");
        return false;
    } // true

    //alert(searchParams.get('symbol'));
    var symbol = searchParams.get('symbol');
    $.get("/api/v1/emacross?symbol="+symbol, function(data, status){
        //alert("Status: " + status);
        //alert(dates);
        //create dates for Y axis, closings for C20 & C50

        if (status = 'success') {
            for (i=Object.keys(data).length-1;i>=0;i--){
                dates.push(data[i]['time']);
                closing20.push(data[i]['Closing_20']);
                closing50.push(data[i]['Closing_50']);
            }
            renderChart(dates,closing20,closing50,symbol);
        }
        
        else {
            alert("API call failed. Status: " + status);
            return false;
        }

    });

    //renderChart(dates,closing20,closing50,symbol);
};
