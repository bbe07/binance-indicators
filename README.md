# Binance Indicators

An Python Flask API with HTML features to display statistics about cryptocurrency trends.
To use, run app.py then make HTTP GET calls to query data from the service with this format-> http://localhost:5000/(api/view)/v1/(endpoint)?symbol=(cryptocurrency pair)

Examples:
<br/>
http://localhost:5000/API/v1/emacross?symbol=BTCUSDT
<br/>
http://localhost:5000/view/v1/emacross?symbol=VETUSDT
