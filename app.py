import flask
from flask import request, jsonify, render_template
import pandas as pd
import numpy as np
import requests
from datetime import datetime, timezone, timedelta

app = flask.Flask(__name__)
app.config["DEBUG"] = True

@app.route('/', methods=['GET'])
def home():
    return render_template('index.html')

@app.route('/view/v1/emacross', methods=['GET'])
def view_emacross():
    if 'symbol' in request.args:
        symbol = str(request.args['symbol']).upper()
        if symbol == '':
            return "Error: No symbol field provided. Please specify a symbol.", 400
    else:
        return "Error: No symbol field provided. Please specify a symbol.", 400
    return render_template('ema.html')

# Sample: http://localhost:5000/api/v1/emacross?symbol=VETUSDT
@app.route('/api/v1/emacross', methods=['GET'])
def api_id():
    # Check if an ID was provided as part of the URL.
    # If ID is provided, assign it to a variable.
    # If no ID is provided, display an error in the browser.
    if 'symbol' in request.args:
        symbol = str(request.args['symbol']).upper()
        if symbol == '':
            return "Error: No symbol field provided. Please specify a Binance compatible symbol.", 400
    else:
        return "Error: No symbol field provided. Please specify a Binance compatible symbol.", 400

    # Create an empty list for our results
    # results = []
    values = []

    # Using the symbol, call binance and calculate the EMA 20 & 50
    # IDs are unique, but other fields might return many results
    headers = {"Content-Type": "application/json","Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"}
    response = requests.get('https://api.binance.com/api/v3/klines?symbol='+symbol+'&interval=1h&limit=220',headers=headers)

    if response.status_code == 200:
        # Continue with Program
        jsonData = response.json()
        for i in jsonData:
            values.append(np.float64(i[4]))
        
        #Calculate the series
        series20 = calculateEma(20,values)
        series50 = calculateEma(50,values)
        
        # Create data object and add 8 hours for MYT
        now = datetime.now(timezone.utc)
        now = now + timedelta(hours=8)
        #now = datetime.now()

        # Write to a new dictionary
        outputDict = {}
        i = 0
        while i < 20:
            runDate = now - timedelta(hours=i)
            theDate = str(runDate.year) + '-' + str(runDate.month) + '-' + str(runDate.day) + ' ' + str(runDate.hour) + ':00'
            #theDate = str(now.year) + '-' + str(now.month) + '-' + str(now.day) + ' ' + str((now -timedelta(hours=i)).hour) + ':00'

            outputDict[i] = {
                'time' : theDate,
                'Closing_20': round(series20["closing_ewma"][219-i],6),
                'Closing_50': round(series50["closing_ewma"][219-i],6)
            }
            i+=1
        
        # print(outputDict)

        # Append to dictionary
        # Create an empty dictionary for our results
        # results = []
        # results = buildDictionary(series20,series50)

        # Prepare the data to be added into the dictionary
        # x = {'Period':20,'Closing':round(series20["closing_ewma"][199],6),'Closing adjust':round(series20["closing_ewma_adjust"][199],6)}
        # y = {'Period':50,'Closing':round(series50["closing_ewma"][199],6),'Closing adjust':round(series50["closing_ewma_adjust"][199],6)}

        # Use the jsonify function from Flask to convert our list of
        # Python dictionaries to the JSON format.

        return jsonify(outputDict)

    else:
        return 'An error has occured during Binance API call. HTTP Status: ' + str(response.status_code)

def buildDictionary(series20,series50):
    # Build the dictionary
    resultDict = [
        {
            'Period': 20,
            'Closing': round(series20["closing_ewma"][199],6),
            'Closing adjust': round(series20["closing_ewma_adjust"][199],6)
        },
        {
            'Period': 50,
            'Closing': round(series50["closing_ewma"][199],6),
            'Closing adjust': round(series50["closing_ewma_adjust"][199],6)
        }
    ]

    return resultDict


def calculateEma(period, values):
	name = 'closing'
	series = pd.Series(values, name=name, dtype=np.float64).to_frame()

	alpha = np.float64(2/(1+period))

	series[name+'_ewma'] = np.nan
	series.loc[0, name+'_ewma'] = series[name].iloc[0]

	series[name+'_ewma_adjust'] = np.nan
	series.loc[0, name+'_ewma_adjust'] = series[name].iloc[0]

	#print(series.loc[0, name+'_ewma']) #get data at location
	for i in range(1, len(series)):
	    series.loc[i, name+'_ewma'] = (1-alpha) * series.loc[i-1, name+'_ewma'] + alpha * series.loc[i, name]
	    ajusted_weights = np.array([(1-alpha)**(i-t) for t in range(i+1)])
	    series.loc[i, name+'_ewma_adjust'] = np.sum(series.iloc[0:i+1][name].values * ajusted_weights) / ajusted_weights.sum()

	#print(series)
	#print("diff adjusted=False -> ", np.sum(series[name+'_ewma'] - series[name].ewm(span=period, adjust=False).mean()))
	#print("diff adjusted=True -> ", np.sum(series[name+'_ewma_adjust'] - series[name].ewm(span=period, adjust=True).mean()))
	#print("Period "+str(period)+" Closing: "+str(round(series["closing_ewma"][199],6))+". Closing Adjust: "+str(round(series["closing_ewma_adjust"][199],6)))

	return series

app.run(host="0.0.0.0", port=int("5000"))
